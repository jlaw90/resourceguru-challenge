### Resource Guru
#### Overview

This repository contains my response to the code challenge
presented as part of my job application to Resource Guru.

Source files for the array flattening and morse
encoding/obfuscation are in `lib/flatten.rb` and `lib/morse.rb`
respectively.

All code is commented in `RDoc` syntax, it is recommended to
generate docs with `yard` if wishing to browse.  The doc directory
is in `.gitignore` as a best-practice to prevent the repository being
littered with non-code artifacts.

Tests are in the `spec` directory, following `RSpec` best-practice and can be run simply by executing
`rspec` in the terminal

#### CLI

The CLI program is available in `./cli.rb`, it can be given
input in multiple ways:
* With no arguments it will enter a
[REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop),
where each line will be encoded, obfuscated and printed to `STDOUT`
* With input piped into `STDIN`
* With one or more file paths passed as arguments, in which case
each file will be encoded, obfuscated and printed in order

#### Feedback
This was a very fun programming challenge, the most challenging
parts for myself were fighting with `RDoc` and
remembering `RSpec` syntax!

There were some fun edge cases requiring to specify how words are
defined and how invalid input is handled, but all are documented
against the function and have tests to verify correct behaviour.

Ruby was chosen simply as it's my favourite programming language,
however I could happily re-write it if needed.

Thanks your consideration
