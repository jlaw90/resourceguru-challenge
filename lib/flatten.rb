# An iterative array flattening function - see git history for original recursive implementation
#
# Given an array such as +[1, [2, [3]], [4], [5]]+ it would return +[1, 2, 3, 4, 5]+
#
# @author James Lawrence
#
# @param [Array] input an array to be flattened
#
# @return [NilClass] if the input is not an +Array+
# @return [Array] the flattened version of the array
def flatten(input)
  return nil unless input.is_a? Array

  output = []

  inputs, indices = [input], [-1]

  until inputs.empty? do
    arr = inputs.pop
    i = indices.pop + 1
    while i < arr.length
      v = arr[i]
      if v.is_a? Array
        inputs.push(arr)
        indices.push(i)
        arr = v
        i = 0
        next
      end

      output.push(v)
      i += 1
    end
  end

  output
end
