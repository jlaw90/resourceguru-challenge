# A +Hash+ of string runes to morse encoding
ASCII_TO_MORSE = {
    ?A => '.-',
    ?B => '-...',
    ?C => '-.-.',
    ?D => '-..',
    ?E => '.',
    ?F => '..-.',
    ?G => '--.',
    ?H => '....',
    ?I => '..',
    ?J => '.---',
    ?K => '-.-',
    ?L => '.-..',
    ?M => '--',
    ?N => '-.',
    ?O => '---',
    ?P => '.--.',
    ?Q => '--.-',
    ?R => '.-.',
    ?S => '...',
    ?T => '-',
    ?U => '..-',
    ?V => '...-',
    ?W => '.--',
    ?X => '-..-',
    ?Y => '-.--',
    ?Z => '--..',
    ?0 => '-----',
    ?1 => '.----',
    ?2 => '..---',
    ?3 => '...--',
    ?4 => '....-',
    ?5 => '.....',
    ?6 => '-....',
    ?7 => '--...',
    ?8 => '---..',
    ?9 => '----.',
    ?. => '.-.-.-',
    ?, => '--..--'
}

# Encodes a string using morse encoding.
#
# @note
#   A word is defined as a string of alphanumeric characters, full-stops and commas are treated as individual words.
#
#   Whitespace will not be preserved during encoding.
#
# @author James Lawrence
#
# @param [String] s The input string.  The input can contain upper-case letters,
#   numbers, spaces, dots and commas (regular expression: +[A-Z\\d,\\.\\s]*+)
#
# @raise [EncodingError] if the string contains invalid characters
#
# @return [NilClass] if the input is not a +String+
#
# @return [String] The morse encoded output, with words separated by a forward slash and letters separated by the pipe symbol
def string_to_morse(s)
  # Return nil on invalid input
  return nil unless s.is_a? String

  words, word = [], []
  # Simplify whitespace handling, get rid of any continuations and replace all with a simple space, then chomp
  s.gsub(/\s+/, ' ').chomp.each_char do |letter|
    # If this is a comma, dot or whitespace, end the previous word (if any)
    if letter == ?, || letter == ?. || letter == ' '
      words << word unless word.empty?
      word = []
    end

    # Continue if this is whitespace...
    next if letter == ' '

    # Get our encoded character
    morse = ASCII_TO_MORSE[letter]
    raise EncodingError.new("not a valid input character: #{letter}") if morse.nil?

    # If this is a comma or dot, add as a new word
    if letter == ?, || letter == ?.
      words << [morse]
      next
    end

    # Otherwise, build our current word
    word << morse
  end

  words << word unless word.empty?

  words.map{|w| w * '|'} * '/'
end

# Takes a morse encoded string in the format defined in +string_to_morse+ and obfuscates it.
#
# Replaces the number of consecutive dots with a number, and replace the number of consecutive dashes with the letter
# of the alphabet at that position. E.g. +S = ... = 3+, +Q = --.- = b1a+, +F = ..-. = 2a1+
#
# @author James Lawrence
#
# @raise [EncodingError] if the input string is in an unexpected format
#
# @see #string_to_morse
#
# @param [String] input the morse encoded input
#
# @return [String] the obfuscated output
# @return [NilClass] if the input is not a +String+
def obfuscate_morse(input)
  return nil unless input.is_a? String

  input.split(/\//).map do |word|
    # Letter here is one more encoded character
    word.split(/\|/).map do |letter|
      # Keep track of our output, what the current run is (dot or dash), and how many we've seen sequentially
      obfuscated = ''
      run_type = letter[0]
      run_count = 1

      # A lambda to update our output and reset run counters,
      # added here so we don't have to repeat ourselves after the loop
      append_run = lambda do |next_char=nil|
        obfuscated += case
                        # runs of dots should be replaced by the number of dots
                        when run_type == '.' then run_count.to_s
                        # runs of dashes should be replaced by the upper-case letter of the alphabet corresponding to the count
                        when run_type == '-' then (?A.ord + run_count - 1).chr
                        # this is a good place to handle invalid input, all except pipes and forward slashes pass here
                        # it does create an edge-case where you could have empty words or letters, but is this a bug or a feature? ;)
                        else raise EncodingError.new("unexpected input to obfuscate_morse: #{run_type}")
                      end
        run_type = next_char
        run_count = 1
      end

      letter[1..-1].each_char do |character|
        if character == run_type
          run_count += 1
          next
        end

        append_run.call(character)
      end

      append_run.call

      obfuscated
      # join the map result back with the pipe
    end * '|'
    # join the map result back with a forward slash
  end * '/'
end
