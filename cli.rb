#!/usr/bin/ruby
#
# This script can accept either input to STDIN, or the path to one or more files
# it will read the entirety of any inputs, and print out the obfuscated output, line-by-line

require_relative './lib/morse'

ARGF.each do |line|
  puts obfuscate_morse(string_to_morse(line))
end
