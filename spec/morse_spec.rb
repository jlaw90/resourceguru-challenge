require 'morse'

describe 'string_to_morse' do
  context 'when given input other than a string' do
    it 'returns nil' do
      expect(string_to_morse(1)).to eq(nil)
      expect(string_to_morse(4..6)).to eq(nil)
      expect(string_to_morse({})).to eq(nil)
      expect(string_to_morse([])).to eq(nil)
    end
  end

  context 'when given a valid input string' do
    it 'returns the expected output' do
      expect(string_to_morse('I AM IN TROUBLE')).to eq('../.-|--/..|-./-|.-.|---|..-|-...|.-..|.')
      expect(string_to_morse('HI')).to eq('....|..')
      expect(string_to_morse('AN EXAMPLE')).to eq('.-|-./.|-..-|.-|--|.--.|.-..|.')
    end

    it 'correctly splits words as defined' do
      expect(string_to_morse('A A')).to eq('.-/.-')
      # symbols are individual words, no matter how they are grouped with other inputs
      expect(string_to_morse(',,')).to eq('--..--/--..--')
      expect(string_to_morse('.A,.')).to eq('.-.-.-/.-/--..--/.-.-.-')

      # whitespace is treated as singular whitespace
      expect(string_to_morse("A\tA")).to eq('.-/.-')
      expect(string_to_morse("A    A")).to eq('.-/.-')
      expect(string_to_morse(" \tA \t  \r  A\n\r ")).to eq('.-/.-')
    end
  end

  context 'when passed invalid characters' do
    it 'raises an EncodingError' do
      expect { string_to_morse('THIS IS INVALID $') }.to raise_error(EncodingError)
    end
  end
end

describe 'obfuscate_morse' do
  context 'when given input that is not a string' do
    it 'returns nil' do
      expect(string_to_morse(1)).to eq(nil)
      expect(string_to_morse(4..6)).to eq(nil)
      expect(string_to_morse({})).to eq(nil)
      expect(string_to_morse([])).to eq(nil)
    end
  end

  context 'when passed invalid characters' do
    it 'raises an EncodingError' do
      expect { obfuscate_morse('....|../you') }.to raise_error(EncodingError)
    end
  end

  context 'when passed valid morse encoded input' do
    it 'returns the correct obfuscated result' do
      expect(obfuscate_morse('../.-|--/..|-./-|.-.|---|..-|-...|.-..|.')).to eq('2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1')
    end
  end
end
