require 'flatten'

describe 'flatten' do
  context 'given a simple nested array' do
    it 'returns the flattened version of the array in order' do
      expect(flatten([1,[2,[3,4], [5,[6,[7]]]], [8, 9]])).to eq([1,2,3,4,5,6,7,8,9])
      expect(flatten([])).to eq([])
      expect(flatten([1,2])).not_to eq([2,1])
    end
  end

  context 'given invalid input' do
    it 'returns nil' do
      expect(flatten('string')).to eq(nil)
      expect(flatten(1)).to eq(nil)
      expect(flatten(1...4)).to eq(nil)
      expect(flatten({})).to eq(nil)
    end
  end
end
